package org.amdatu.remoteservices.discovery.bonjour.impl;

import java.util.Properties;
import java.util.UUID;

import org.amdatu.remoteservices.discovery.commands.Discovery;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.remoteserviceadmin.EndpointListener;
import org.osgi.service.remoteserviceadmin.RemoteConstants;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {
		Properties properties = new Properties();
		properties.put("discovery", true);
		properties.setProperty(EndpointListener.ENDPOINT_LISTENER_SCOPE, 
				"(&(" + Constants.OBJECTCLASS + "=*)(" + RemoteConstants.ENDPOINT_FRAMEWORK_UUID + "=" + getUUID(context) + "))");
		
		manager.add(createComponent()
			.setImplementation(DiscoveryBonjour.class)
			.setInterface(new String[] { EndpointListener.class.getName(), Discovery.class.getName() }, properties)
			.add(createServiceDependency()
				.setService(EndpointListener.class)
				.setCallbacks("endpointListenerAdded", "endpointListenerModified", "endpointListenerRemoved")));
	}
	
	@Override
	public void destroy(BundleContext context, DependencyManager manager) throws Exception {
		
	}
	
	private String getUUID(BundleContext bctx) {
        synchronized ("org.osgi.framework.uuid") {
            String uuid = bctx.getProperty("org.osgi.framework.uuid");
            if(uuid==null){
                uuid = UUID.randomUUID().toString();
                System.setProperty("org.osgi.framework.uuid", uuid);
            }
            return uuid;
        }
    }

}
