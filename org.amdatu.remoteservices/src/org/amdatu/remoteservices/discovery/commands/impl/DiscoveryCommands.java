package org.amdatu.remoteservices.discovery.commands.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.amdatu.remoteservices.discovery.commands.Discovery;
import org.osgi.framework.ServiceReference;
import org.osgi.service.remoteserviceadmin.EndpointDescription;

public class DiscoveryCommands {
	
	private Map<ServiceReference, Discovery> m_discoveries = new HashMap<ServiceReference, Discovery>();
	
	public void discoveryAdded(ServiceReference reference, Discovery discovery) {
		m_discoveries.put(reference, discovery);
	}
	
	public void discoveryRemoved(ServiceReference reference, Discovery discovery) {
		m_discoveries.remove(reference);		
	}
	
	/** Shell command to list all exported endpoints. */
	public void exported() {
		for (Entry<ServiceReference, Discovery> entry : m_discoveries.entrySet()) {
			ServiceReference reference = entry.getKey();
			Discovery discovery = entry.getValue();
			
			String header = reference.getBundle().getSymbolicName() + " [" + reference.getBundle().getBundleId() + "] exports:";
	        printHeader(header);
	        
	        List<EndpointDescription> exportedEndpoints = discovery.getExportedEndpoints();
	        printEndpointDescriptions(exportedEndpoints);
		}
	} 
	
    /** Shell command to list all imported endpoints. */
	public void imported() {
		for (Entry<ServiceReference, Discovery> entry : m_discoveries.entrySet()) {
			ServiceReference reference = entry.getKey();
			Discovery discovery = entry.getValue();
			
			String header = reference.getBundle().getSymbolicName() + " [" + reference.getBundle().getBundleId() + "] imports:";
	        printHeader(header);
	        
	        List<EndpointDescription> importedEndpoints = discovery.getImportedEndpoints();
	        printEndpointDescriptions(importedEndpoints);
		}
	}
	
	private void printHeader(String header) {
		System.out.println(header);
        StringBuilder underline = new StringBuilder();
        for (int i = 0; i < header.length(); i++) {
        	underline.append("-");
        }
        System.out.println(underline);
	}
	
	private void printEndpointDescriptions(List<EndpointDescription> endpointDescriptions) {
	    if (endpointDescriptions.isEmpty()) {
            System.out.println("endpoint [EMPTY]");
        } else {
            for (EndpointDescription endpoint : endpointDescriptions) {
                System.out.println("endpoint; " + endpoint.getId() + " with properties:");
                Map<String, Object> properties = endpoint.getProperties();
                for (Entry<String, Object> property : properties.entrySet()) {
                	Object value = property.getValue();
                	if (value instanceof String[]) {
                		System.out.println("   " + property.getKey() + " = " + Arrays.toString((String[]) value));
                	} else {
                		System.out.println("   " + property.getKey() + " = " + property.getValue().toString());
                	}
				}
            }
        }
	    System.out.println("");
	}
}