package org.amdatu.remoteservices.remoteserviceadmin.impl;

import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.ExportReference;
import org.osgi.service.remoteserviceadmin.ExportRegistration;

public class ExportRegistrationImpl implements ExportRegistration {
	
	private final EndpointDescription m_endpointDescription;
	private final ServiceReference m_serviceReference;
	private final RemoteServiceAdminImpl m_remoteServiceAdminImpl;
	private final ServiceRegistration m_serverEndpointRegistration;
	private final Throwable m_exception;
	private final ExportRegistrationImpl m_parent;
	
	private ExportReference m_exportReference;

	private boolean m_closed;
	
	private int m_instanceCount = 1;
	
	public ExportRegistrationImpl(ServiceReference reference, EndpointDescription endpointDescription, RemoteServiceAdminImpl remoteServiceAdminImpl, ServiceRegistration serverEndpointRegistration) {	
		m_serviceReference = reference;
		m_endpointDescription = endpointDescription;
		m_remoteServiceAdminImpl = remoteServiceAdminImpl;
		m_serverEndpointRegistration = serverEndpointRegistration;
		m_exception = null;
		m_parent = this;
		
		m_closed = false;
	}
	
	public ExportRegistrationImpl(Throwable exception) {
		m_exception = exception;
		m_serviceReference = null;
		m_endpointDescription = null;
		m_remoteServiceAdminImpl = null;
		m_exportReference = null;
		m_serverEndpointRegistration=null;
		m_parent = this;
	}

	public ExportRegistrationImpl(ExportRegistrationImpl exportRegistration) {
		m_parent = exportRegistration;
		m_serviceReference = m_parent.getExportedService();
		m_endpointDescription = m_parent.getEndpointDescription();
		m_remoteServiceAdminImpl = m_parent.getRemoteServiceAdmin();
		m_exception = m_parent.getException();
		
		// Only the top level parent holds the registration
		m_serverEndpointRegistration = null;
		
		m_parent.instanceAdded();
	}

	@Override
	public void close() {
		if (!m_closed) { 
			m_closed = true;
			
			m_remoteServiceAdminImpl.removeExport(this);
			
			m_parent.instanceClosed();
		}
	}

	@Override
	public Throwable getException() {
		if (m_closed) {
			return null;
		} else {
			return m_exception;
		}
	}

	@Override
	public ExportReference getExportReference() {
		if (m_exportReference == null) {
			m_exportReference = new ExportReferenceImpl(m_serviceReference, m_endpointDescription);
		}
		
		return m_exportReference;
	}
	
	public void instanceAdded() {
		++m_instanceCount;
	}
	
	public void instanceClosed() {
		--m_instanceCount;
		
		if (m_instanceCount <= 0) {
			synchronized (this) {
				if (m_serverEndpointRegistration != null) {
					m_serverEndpointRegistration.unregister();
				}
			}
		}
	}

	public ServiceRegistration getServerEndpointRegistration() {
		return m_serverEndpointRegistration;
	}
	
	public ServiceReference getExportedService() {
		return m_serviceReference;
	}
	
	public EndpointDescription getEndpointDescription() {
		return m_endpointDescription;
	}
	
	public RemoteServiceAdminImpl getRemoteServiceAdmin() {
		return m_remoteServiceAdminImpl;
	}
}
