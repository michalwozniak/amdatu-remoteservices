package org.amdatu.remoteservices.test.topologymanager.impl;

import org.amdatu.remoteservices.test.topologymanager.Test;

public class TestImpl implements Test {

	@Override
	public void printMessage(String data) {
		System.out.println("TestImpl printMessage called: " + data);
	}

}
