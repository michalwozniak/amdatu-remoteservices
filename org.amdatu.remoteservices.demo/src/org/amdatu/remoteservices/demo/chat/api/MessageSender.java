package org.amdatu.remoteservices.demo.chat.api;

public interface MessageSender {

	void send(String message);
}
