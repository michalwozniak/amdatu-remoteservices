package org.amdatu.remoteservices.test.topologymanager.impl;

import java.util.Properties;

import org.amdatu.remoteservices.test.topologymanager.Test;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.remoteserviceadmin.RemoteConstants;


public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext arg0, DependencyManager manager)
			throws Exception {
		Properties props = new Properties();
		props.setProperty(RemoteConstants.SERVICE_EXPORTED_INTERFACES, Test.class.getName());
		//props.setProperty(".slp.scopes", "a,b,c");
		
		manager.add(createComponent().setImplementation(TestImpl.class).setInterface(Test.class.getName(), props));
	}
	
	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1)
			throws Exception {
		
	}
}
