package org.amdatu.remoteservices.demo.chat.impl;

import java.util.LinkedList;
import java.util.List;

import org.amdatu.remoteservices.demo.chat.api.MessageReceiver;
import org.amdatu.remoteservices.demo.chat.api.MessageSender;
import org.apache.felix.dm.Component;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;

public class ChatClient implements MessageSender, MessageReceiver {

	private final List<ChatPeer> m_peers = new LinkedList<ChatPeer>();

	private volatile Component m_component;

	private String m_name;

	@Override
	public void receive(String message) {
		System.out.println(message);
	}

	@Override
	public void send(String message) {
		synchronized (m_peers) {
			for (ChatPeer peer : m_peers) {
				peer.getMessageReceiver().receive(message);
			}
		}
	}

	void start() {
		m_name = (String) m_component.getServiceProperties().get("chat.name");
		System.out.println("Starting " + m_name);
	}

	void addReceiver(ServiceReference reference,
			MessageReceiver receiver) {

		if (receiver == this)
			return;

		String chatName = (String) reference.getProperty("chat.name");
		if (chatName == null || "".equals(chatName))
			chatName = "peer " + reference.getProperty(Constants.SERVICE_ID);

		synchronized (m_peers) {
			m_peers.add(new ChatPeer(chatName, receiver));
		}
		System.out.println("> " + chatName + " joined the room");

		receiver.receive("Hello! " + chatName + " from " + m_name);
	}

	void removeReceiver(MessageReceiver receiver) {

		if (receiver == this)
			return;

		ChatPeer peer = null;
		synchronized (m_peers) {
			for (ChatPeer chatpeer : m_peers) {
				if (chatpeer.getMessageReceiver() == receiver) {
					peer = chatpeer;
					break;
				}
			}
			if (peer != null)
				m_peers.remove(peer);
		}
		if (peer != null)
			System.out.println("> " + peer.getName() + " left the room");
	}

	static class ChatPeer {

		private final String m_name;
		private final MessageReceiver m_receiver;

		public ChatPeer(String name, MessageReceiver receiver) {
			m_name = name;
			m_receiver = receiver;
		}

		public String getName() {
			return m_name;
		}

		public MessageReceiver getMessageReceiver() {
			return m_receiver;
		}

	}
}
