package org.amdatu.remoteservices.discovery.slp;

/**
 * SLP Configuration properties used for service registrations.
 * 
 * The OSGi RSA specification details how configuration types can be used to decide what type of Endpoint is created, 
 * but there is no default way to tailor the discovery registration.
 * These properties can be used to influence how a service is registered using SLP. 
 * The SLPDiscovery implementation takes the settings from the Endpoint properties, so it is possible by the user to add
 * the properties to the service registration, since these are added to the Endpoint properties.
 *  
 * @author alexander
 *
 */
public interface SlpDiscoveryConstants {

	public static final String SLP_CONFIG_TYPE = ".slp"; // SLP discovery configuration type
	public static final String SLP_SCOPES = SLP_CONFIG_TYPE + ".scopes";
	public static final String SLP_LANGUAGUE = SLP_CONFIG_TYPE + ".language";
	public static final String SLP_LIFETIME = SLP_CONFIG_TYPE + ".lifetime";
	
}
