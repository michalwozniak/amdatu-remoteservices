package org.amdatu.remoteservices.remoteserviceadmin.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.Servlet;

import org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl.RestClientEndpoint;
import org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl.RestServerEndpoint;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.log.LogService;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.ExportReference;
import org.osgi.service.remoteserviceadmin.ExportRegistration;
import org.osgi.service.remoteserviceadmin.ImportReference;
import org.osgi.service.remoteserviceadmin.ImportRegistration;
import org.osgi.service.remoteserviceadmin.RemoteConstants;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdmin;


public class RemoteServiceAdminImpl implements RemoteServiceAdmin {
	
	private volatile BundleContext m_context;
	private volatile LogService m_log;
	
	private Map<ServiceReference, Collection<ExportRegistrationImpl>> m_exportedServices;
	private Map<EndpointDescription, Collection<ImportRegistrationImpl>> m_importedServices;
	
	private static final String ARS_CONFIG_TYPE = ".ars"; // Amdatu Remote Service service type
	private static final String ARS_ALIAS = ARS_CONFIG_TYPE + ".alias"; // ARS alias
	
	public RemoteServiceAdminImpl() {
		m_exportedServices = new HashMap<ServiceReference, Collection<ExportRegistrationImpl>>();
		m_importedServices = new HashMap<EndpointDescription, Collection<ImportRegistrationImpl>>();
	}
	
	@Override
	public Collection<ExportRegistration> exportService(ServiceReference reference, Map<String, Object> properties) {
		Collection<ExportRegistrationImpl> exports = null;
		
		synchronized (m_exportedServices) {
			if (m_exportedServices.containsKey(reference)) {
				m_log.log(LogService.LOG_INFO, "Service '" + reference.getProperty(Constants.OBJECTCLASS) + "' is already exported, creating copy.");
				exports = new ArrayList<ExportRegistrationImpl>();
				
				Collection<ExportRegistrationImpl> existingExports = m_exportedServices.get(reference);
				for (ExportRegistration exportRegistration : existingExports) {
					exports.add(new ExportRegistrationImpl((Throwable) exportRegistration));
				}
			} else if (reference.getBundle().equals(m_context.getBundle())) {
				m_log.log(LogService.LOG_INFO, "Trying to export a service created by this bundle, ignoring");
				exports = Collections.emptyList();
			} else {
				String[] exportedInterfaces = getServiceExportedIntefaces(reference);
				String[] providedInterfaces = (String[]) reference.getProperty(Constants.OBJECTCLASS);
				
				if (exportedInterfaces == null || providedInterfaces == null) {
					m_log.log(LogService.LOG_ERROR, "No services to export");
					
					exports = Collections.emptyList();
				} else {
					List<String> interfaces = new ArrayList<String>();
					
					if (exportedInterfaces.length == 1 && exportedInterfaces[0].equals("*")) {
						for (String interf : providedInterfaces) {
							interfaces.add(interf);
						}
					} else {
						for (String exported : exportedInterfaces) {
							for (String provided : providedInterfaces) {
								if (exported.equals(provided)) {
									interfaces.add(provided);
								}
							}
						}
					}
					
					if (interfaces.size() == 0) {
						m_log.log(LogService.LOG_ERROR, "No services to export");
						
						exports = Collections.emptyList();
					} else {
						exports = new ArrayList<ExportRegistrationImpl>();
						
						for (String exportInterface : interfaces) {
							ExportRegistrationImpl er = exportServiceAsJsonRest(reference, properties, exportInterface);
							if (er.getExportReference() != null) {
								exports.add(er);
							}
						}
						
						m_exportedServices.put(reference, exports);
					}
				}
			}
		}
		
		return new ArrayList<ExportRegistration>(exports);
	}
	
	private String[] getServiceExportedIntefaces(ServiceReference ref) {
		String[] result = null;
		Object propValue = ref.getProperty(RemoteConstants.SERVICE_EXPORTED_INTERFACES);
		if (propValue instanceof String) {
			result = new String[] {(String)propValue};
		} else if (propValue instanceof String[] ) {
			result = (String[])propValue;
		} else if (propValue instanceof Collection<?>) {
			result = ((Collection<?>)propValue).toArray(new String[0]);
		}
		return result;
	}
	
	private ExportRegistrationImpl exportServiceAsJsonRest(ServiceReference reference, Map<String, Object> properties, String interfaceName) {
		ExportRegistrationImpl result = null;
		
		try {
			RestServerEndpoint endpoint = new RestServerEndpoint(m_context, reference, new String[] {interfaceName});
			Hashtable<String, Object> props = properties == null ? new Hashtable<String, Object>() : new Hashtable<String, Object>(properties);
			String alias = "/service/" + reference.getProperty("service.id") + "/" + interfaceName;
			props.put("alias", alias);
			System.out.println("REMOTE_SERVICE_ADMIN: Registered service with alias " + alias);
			ServiceRegistration reg = m_context.registerService(Servlet.class.getName(), endpoint, props);
			
			Hashtable<String, Object> descriptionProps = new Hashtable<String, Object>();

			String[] propertyKeys = reference.getPropertyKeys();
			for (String key : propertyKeys) {
				if (!key.equals(Constants.SERVICE_ID)) {
					descriptionProps.put(key, reference.getProperty(key));
				}
			}
			
			String frameworkUUID = getUUID(m_context);
			descriptionProps.put(Constants.OBJECTCLASS, new String[] {interfaceName});
			descriptionProps.put(RemoteConstants.ENDPOINT_ID, frameworkUUID + alias);
			descriptionProps.put(RemoteConstants.ENDPOINT_SERVICE_ID, reference.getProperty(Constants.SERVICE_ID));
            descriptionProps.put(RemoteConstants.ENDPOINT_FRAMEWORK_UUID, frameworkUUID);
			descriptionProps.put(RemoteConstants.SERVICE_IMPORTED_CONFIGS, new String[] { ARS_CONFIG_TYPE });
			String port = m_context.getProperty("org.osgi.service.http.port");
			if (port == null) {
				port = "8080";
			}
			String host = m_context.getProperty("host");
			descriptionProps.put(ARS_ALIAS, "http://" + host + ":" + port + alias);
			descriptionProps.put(".ars.path", alias);
			descriptionProps.put(".ars.port", port);
			EndpointDescription endpointDescription = new EndpointDescription(reference, descriptionProps);
			result = new ExportRegistrationImpl(reference, endpointDescription, this, reg);
		} catch (ClassNotFoundException e) {
			result = new ExportRegistrationImpl(e);
		}
		
		return result;
	}
	
	@Override
	public ImportRegistration importService(EndpointDescription endpoint) {
		ImportRegistrationImpl importRegistration = null;
		synchronized (m_importedServices) {
			if (m_importedServices.containsKey(endpoint)) {
				m_log.log(LogService.LOG_INFO, "Endpint '" + endpoint.getId() + "' is already imported, creating copy.");
				
				Collection<ImportRegistrationImpl> imports = m_importedServices.get(endpoint);
				ImportRegistrationImpl parent = imports.iterator().next();
				importRegistration = new ImportRegistrationImpl(parent);
				imports.add(importRegistration);
			} else {
				List<String> configurationTypes = endpoint.getConfigurationTypes();
				// For now we only support the default ARS_CONFIG_TYPE
				if (!configurationTypes.contains(ARS_CONFIG_TYPE)) {
					m_log.log(LogService.LOG_INFO, "No supported configuratin type found, ignored endpoint");
				} else {
					try {
						String serviceLocation = (String) endpoint.getProperties().get(ARS_ALIAS);
						RestClientEndpoint clientEndpoint = new RestClientEndpoint(endpoint.getInterfaces().toArray(new String[0]), serviceLocation);
						Object service = clientEndpoint.getServiceProxy();
						ServiceRegistration registration = m_context.registerService(endpoint.getInterfaces().toArray(new String[0]), service, null);
						importRegistration = new ImportRegistrationImpl(registration.getReference(), endpoint, this, registration);				
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
						importRegistration = new ImportRegistrationImpl(e);
					}
				}
				
			}
		}
		
		return importRegistration;
	}

	@Override
	public Collection<ExportReference> getExportedServices() {
		synchronized (m_exportedServices) {
            List<ExportReference> ers = new ArrayList<ExportReference>();
            for (Collection<ExportRegistrationImpl> exportRegistrations : m_exportedServices.values()) {
                for (ExportRegistrationImpl er : exportRegistrations) {
                    ers.add(er.getExportReference());
                }
            }
            return Collections.unmodifiableCollection(ers);
        }
	}

	@Override
	public Collection<ImportReference> getImportedEndpoints() {
		synchronized (m_importedServices) {
			List<ImportReference> imports = new ArrayList<ImportReference>();
			for (Collection<ImportRegistrationImpl> importRegistrations : m_importedServices.values()) {
				for (ImportRegistrationImpl importRegistration : importRegistrations) {
					imports.add(importRegistration.getImportReference());
				}
			}
			return Collections.unmodifiableCollection(imports);
		}
	}

	public void removeExport(ExportRegistrationImpl exportRegistration) {
		synchronized (m_exportedServices) {
            Collection<ExportRegistrationImpl> registrations = m_exportedServices.get(exportRegistration.getExportedService());
            if (registrations != null && registrations.contains(exportRegistration)) {
            	registrations.remove(exportRegistration);
            }
            if (registrations == null || registrations.size() == 0) {
                m_exportedServices.remove(exportRegistration.getExportedService());
            }

        }
	}
	
	public void removeImport(ImportRegistrationImpl importRegistration) {
		synchronized (m_importedServices) {
            Collection<ImportRegistrationImpl> registrations = m_importedServices.get(importRegistration.getEndpoint());
            if (registrations != null && registrations.contains(importRegistration)) {
            	registrations.remove(importRegistration);
            }
            if (registrations == null || registrations.size() == 0) {
                m_exportedServices.remove(importRegistration.getEndpoint());
            }

        }
	}
	
	private String getUUID(BundleContext bctx) {
        synchronized ("org.osgi.framework.uuid") {
            String uuid = bctx.getProperty("org.osgi.framework.uuid");
            if(uuid==null){
                uuid = UUID.randomUUID().toString();
                System.setProperty("org.osgi.framework.uuid", uuid);
            }
            return uuid;
        }
    }

}

