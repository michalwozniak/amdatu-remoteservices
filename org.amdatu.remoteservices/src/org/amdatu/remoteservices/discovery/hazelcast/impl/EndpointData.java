package org.amdatu.remoteservices.discovery.hazelcast.impl;

import java.io.Serializable;
import java.util.Map;

import org.osgi.service.remoteserviceadmin.RemoteConstants;

import com.hazelcast.core.Member;

public class EndpointData implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -589019249454265936L;
	
	private String m_id;
	private Map<String, Object> m_properties;

	private Member m_owner;

	public EndpointData(Map<String, Object> properties, Member owner) {
		m_properties = properties;
		m_owner = owner;
		
		m_id = (String) properties.get(RemoteConstants.ENDPOINT_ID);
	}
	
	public String getId() {
		return m_id;
	}
	
	public Map<String, Object> getProperties() {
		return m_properties;
	}
	
	public Member getOwner() {
		return m_owner;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((m_id == null) ? 0 : m_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EndpointData other = (EndpointData) obj;
		if (m_id == null) {
			if (other.m_id != null)
				return false;
		} else if (!m_id.equals(other.m_id))
			return false;
		return true;
	}
	
	
}
