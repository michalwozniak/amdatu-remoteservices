package org.amdatu.remoteservices.discovery.slp.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.amdatu.remoteservices.discovery.commands.Discovery;
import org.amdatu.remoteservices.discovery.slp.SlpDiscoveryConstants;
import org.livetribe.slp.Attributes;
import org.livetribe.slp.Attributes.Value;
import org.livetribe.slp.SLP;
import org.livetribe.slp.Scopes;
import org.livetribe.slp.ServiceInfo;
import org.livetribe.slp.ServiceType;
import org.livetribe.slp.ServiceURL;
import org.livetribe.slp.sa.ServiceAgent;
import org.livetribe.slp.settings.Keys;
import org.livetribe.slp.settings.MapSettings;
import org.livetribe.slp.settings.Settings;
import org.livetribe.slp.ua.UserAgentClient;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.EndpointListener;
import org.osgi.service.remoteserviceadmin.RemoteConstants;

/**
 * SLP based discovery implementation for use with OSGi Remote Service Admin.
 * This implementation is based on SLP and use the LiveTribe implementation.
 * 
 * 
 * @author alexander
 *
 */
public class DiscoverySLP implements EndpointListener, ManagedService, Discovery {
	
	private static String ARS_SLP_QUERY_SCOPES = "ars.slp.query.scopes";
	private static String ARS_SLP_POLL_INTERVAL = "ars.slp.poll.interval";
	
	private BundleContext m_bundleContext;
	private ServiceAgent m_serviceAgent;
	private ClassLoader m_classLoader;
	
	private Map<ServiceReference, EndpointListener> m_listeners = new HashMap<ServiceReference, EndpointListener>();
	
	private Map<ServiceURL, EndpointDescription> m_importedEndpoints = new HashMap<ServiceURL, EndpointDescription>();
	private Map<EndpointDescription, ArrayList<ServiceInfo>> m_exportedEndpoints = new HashMap<EndpointDescription, ArrayList<ServiceInfo>>();
	
	private ScheduledExecutorService m_slpPoller = Executors.newSingleThreadScheduledExecutor();
	
	private Settings m_slpAgentSettings;
	private Settings m_slpUserSettings;
	
	private Integer m_pollInterval = 30;
	
	/**
	 * Starts the discovery by creating a new Executor polling for SLP services.
	 * To be able to correctly utilize the LiveTribe implementation the ContextClassLoader has to be set.
	 */
	@Override
	public void updated(@SuppressWarnings("rawtypes") Dictionary properties) throws ConfigurationException {
		if (properties != null) {
			if (m_slpAgentSettings == null) {
				m_slpAgentSettings = new MapSettings();
			}
			if (m_slpUserSettings == null) {
				m_slpUserSettings = new MapSettings();
			}
			
			m_pollInterval = Integer.parseInt((String) properties.get(ARS_SLP_POLL_INTERVAL));
			
			Object slpPort = properties.get(Keys.PORT_KEY.getKey());
			if (slpPort != null) {
				m_slpAgentSettings.put(Keys.PORT_KEY, Keys.PORT_KEY.convert(slpPort));
				m_slpUserSettings.put(Keys.PORT_KEY, Keys.PORT_KEY.convert(slpPort));
			}
			Object slpScopes = properties.get(Keys.SCOPES_KEY.getKey());
			if (slpScopes != null) {
				m_slpAgentSettings.put(Keys.SCOPES_KEY, Keys.SCOPES_KEY.convert(slpScopes));
				m_slpUserSettings.put(Keys.SCOPES_KEY, Keys.SCOPES_KEY.convert(slpScopes));
			}
			Object slpAddress = properties.get(Keys.ADDRESSES_KEY.getKey());
			if (slpAddress != null) {
				m_slpAgentSettings.put(Keys.ADDRESSES_KEY, Keys.ADDRESSES_KEY.convert(slpAddress));
				m_slpUserSettings.put(Keys.ADDRESSES_KEY, Keys.ADDRESSES_KEY.convert(slpAddress));
			}
			Object slpMtu = properties.get(Keys.MAX_TRANSMISSION_UNIT_KEY.getKey());
			if (slpMtu != null) {
				m_slpAgentSettings.put(Keys.MAX_TRANSMISSION_UNIT_KEY, Keys.MAX_TRANSMISSION_UNIT_KEY.convert(slpMtu));
				m_slpUserSettings.put(Keys.MAX_TRANSMISSION_UNIT_KEY, Keys.MAX_TRANSMISSION_UNIT_KEY.convert(slpMtu));
			}
			Object tcp = properties.get(Keys.UA_UNICAST_PREFER_TCP.getKey());
			if (tcp != null) {
				m_slpAgentSettings.put(Keys.UA_UNICAST_PREFER_TCP, Keys.UA_UNICAST_PREFER_TCP.convert(tcp));
				m_slpUserSettings.put(Keys.UA_UNICAST_PREFER_TCP, Keys.UA_UNICAST_PREFER_TCP.convert(tcp));
			}
			
			try {
				ClassLoader ccl = Thread.currentThread().getContextClassLoader();
				m_classLoader = this.getClass().getClassLoader();
	            Thread.currentThread().setContextClassLoader(m_classLoader);
	            
	            if (m_serviceAgent != null && m_serviceAgent.isRunning()) {
	            	m_serviceAgent.stop();
	            }
	            if (m_slpPoller != null) {
	            	m_slpPoller.shutdown();
	            	while (!m_slpPoller.isShutdown()) {
	            		TimeUnit.MILLISECONDS.sleep(100);
	            	}
	            	m_slpPoller = Executors.newSingleThreadScheduledExecutor();
	            }
	            
	            m_serviceAgent = SLP.newServiceAgent(m_slpAgentSettings);
				m_serviceAgent.start();
				
				m_slpPoller.scheduleWithFixedDelay(pollSLP(),  0, m_pollInterval, TimeUnit.SECONDS);
		
				Thread.currentThread().setContextClassLoader(ccl);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void start() {
		
	}
	
	public void stop() {
		m_slpPoller.shutdown();
		m_serviceAgent.stop();
	}
	
	private ServiceInfo createServiceInfo(EndpointDescription endpoint, String ipAddress) {
		String port = m_bundleContext.getProperty("org.osgi.service.http.port");
		if (port == null) {
			port = "8080";
		}
		String service = endpoint.getId().substring(endpoint.getFrameworkUUID().length());
		
		String url = "service:osgi.remote:http://" + ipAddress + ":" + port + service;
		ServiceURL serviceURL = new ServiceURL(url, 10);
        String language = Locale.ENGLISH.getLanguage();
        Scopes scopes = Scopes.DEFAULT;
        
        String scopesS = (String) endpoint.getProperties().get(SlpDiscoveryConstants.SLP_SCOPES);
        if (scopesS != null) {
        	scopes = Scopes.from(scopesS.split(","));
        }
        
        Map<String, String> props = new HashMap<String, String>();
        for (String key : endpoint.getProperties().keySet()) {
			if (RemoteConstants.SERVICE_IMPORTED_CONFIGS.equals(key)) {
				String value = join(endpoint.getConfigurationTypes(), ",");
				props.put(key, value);
			} else if (RemoteConstants.ENDPOINT_FRAMEWORK_UUID.equals(key)) {
				props.put(key, endpoint.getFrameworkUUID());
			} else if (RemoteConstants.ENDPOINT_ID.equals(key)) {
				props.put(key, endpoint.getId());
			} else if (RemoteConstants.ENDPOINT_SERVICE_ID.equals(key)) {
				props.put(key, String.valueOf(endpoint.getServiceId()));
			} else if (Constants.OBJECTCLASS.equals(key)) {
				props.put(key, join(endpoint.getInterfaces(), ","));
			} else if (key.equals(Constants.SERVICE_ID)){
				props.put(key, String.valueOf(endpoint.getServiceId()));
			} else { 
				Object obj = endpoint.getProperties().get(key);
				String configurationSetting = Attributes.bytesToOpaque(toByteArray(obj));
				props.put(key, configurationSetting);
			}
		}
        
        Attributes attributes = Attributes.from(props);
        
        return new ServiceInfo(serviceURL, language, scopes, attributes);
	}
	
	public byte[] toByteArray (Object obj)
	{
	  byte[] bytes = null;
	  ByteArrayOutputStream bos = new ByteArrayOutputStream();
	  try {
	    ObjectOutputStream oos = new ObjectOutputStream(bos); 
	    oos.writeObject(obj);
	    oos.flush(); 
	    oos.close(); 
	    bos.close();
	    bytes = bos.toByteArray ();
	  }
	  catch (IOException ex) {
	    //TODO: Handle the exception
	  }
	  return bytes;
	}
	    
	public Object toObject (byte[] bytes)
	{
	  Object obj = null;
	  try {
	    ByteArrayInputStream bis = new ByteArrayInputStream (bytes);
	    ObjectInputStream ois = new ObjectInputStream (bis);
	    obj = ois.readObject();
	  }
	  catch (IOException ex) {
	    //TODO: Handle the exception
	  }
	  catch (ClassNotFoundException ex) {
	    //TODO: Handle the exception
	  }
	  return obj;
	}
	
	
	
	public String join(Iterable<? extends CharSequence> s, String delimiter) {
	    Iterator<? extends CharSequence> iter = s.iterator();
	    if (!iter.hasNext()) return "";
	    StringBuilder buffer = new StringBuilder(iter.next());
	    while (iter.hasNext()) buffer.append(delimiter).append(iter.next());
	    return buffer.toString();
	}
	
	@Override
	public void endpointAdded(EndpointDescription endpoint, String matchedFilter) {
		System.out.println("DISCOVERY: Add endpoint with filter: " + matchedFilter);
		
		for (Inet4Address address : getInet4Addresses()) {
			String ip = address.getHostAddress();
			
			
			ClassLoader ccl = Thread.currentThread().getContextClassLoader();
            Thread.currentThread().setContextClassLoader(m_classLoader);
			
            ServiceInfo serviceInfo = createServiceInfo(endpoint, ip);
            System.out.println("DISCOVERY: Registering new service \"" + serviceInfo.getServiceURL() + "\"");
            
            ArrayList<ServiceInfo> infos = m_exportedEndpoints.get(endpoint);
            if (infos == null) {
            	infos = new ArrayList<ServiceInfo>();
            	m_exportedEndpoints.put(endpoint, infos);
            }
            infos.add(serviceInfo);
            
            m_serviceAgent.register(serviceInfo);
            
            Thread.currentThread().setContextClassLoader(ccl);
		}
	}
	
	@Override
	public void endpointRemoved(EndpointDescription endpoint, String matchedFilter) {
		System.out.println("DISCOVERY: Remove endpoint with filter: " + matchedFilter);
		
		ClassLoader ccl = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(m_classLoader);
        
        ArrayList<ServiceInfo> serviceInfos = m_exportedEndpoints.get(endpoint);
        for (ServiceInfo serviceInfo : serviceInfos) {
        	System.out.println("DISCOVERY: Unregister: " + serviceInfo.getServiceURL());
        	m_serviceAgent.deregister(serviceInfo.getServiceURL(), serviceInfo.getLanguage());
		}
        
        Thread.currentThread().setContextClassLoader(ccl);
	}

	public void endpointListenerAdded(ServiceReference reference, EndpointListener listener) {
		Boolean isDiscovery = (Boolean) reference.getProperty("discovery");
		if (isDiscovery == null) {
			isDiscovery = false;
		}
		if (!isDiscovery) {
			m_listeners.put(reference, listener);
			
			for (EndpointDescription description : m_importedEndpoints.values()) {
				informListenerOfAddition(reference, description);
			}
		}
	}
	
	public void endpointListenerModified(ServiceReference reference, EndpointListener listener) {
		Boolean isDiscovery = (Boolean) reference.getProperty("discovery");
		if (isDiscovery == null) {
			isDiscovery = false;
		}
		if (!isDiscovery) {
			m_listeners.put(reference, listener);
			System.out.println("Modified");
			for (EndpointDescription description : m_importedEndpoints.values()) {
				informListenerOfAddition(reference, description);
			}
		}
	}		
	
	public void endpointListenerRemoved(ServiceReference reference, EndpointListener listener) {
		m_listeners.remove(reference);
	}
	
	private void informListenersOfAddition(EndpointDescription description) {
		for (ServiceReference reference : m_listeners.keySet()) {
			informListenerOfAddition(reference, description);
		}
	}
	
	private void informListenerOfAddition(ServiceReference reference, EndpointDescription description) {
		EndpointListener listener = m_listeners.get(reference);

		String[] scopes = propertyToStringArray(reference.getProperty(ENDPOINT_LISTENER_SCOPE));
		for (String string : scopes) {
			try {
				Filter filter = m_bundleContext.createFilter(string);
				Dictionary<String, Object> properties = new Hashtable<String, Object>(description.getProperties());
				if (filter != null && filter.match(properties)) {
					listener.endpointAdded(description, filter.toString());
				}
			} catch (InvalidSyntaxException e) {
				System.out.println("DISCOVERY: Syntax incorrect, scope ignored: " + e.getMessage());
			}
		}
	}
	
	private void informListenersOfRemoval(EndpointDescription description) {
 		for (ServiceReference reference : m_listeners.keySet()) {
			EndpointListener listener = m_listeners.get(reference);
			
			String[] scopes = propertyToStringArray(reference.getProperty(ENDPOINT_LISTENER_SCOPE));
			for (String string : scopes) {
				try {
					Filter filter = m_bundleContext.createFilter(string);
					Dictionary<String, Object> properties = new Hashtable<String, Object>(description.getProperties());
					if (filter != null && filter.match(properties)) {
						listener.endpointRemoved(description, filter.toString());
					}
				} catch (InvalidSyntaxException e) {
				}
			}
			
		}
	}
	
	private String[] propertyToStringArray(Object property) {

        if (property instanceof String) {
            String[] ret = new String[1];
            ret[0] = (String)property;
            return ret;
        }

        if (property instanceof String[]) {
            return (String[])property;
        }

        if (property instanceof Collection) {
            Collection<?> col = (Collection<?>) property;
            // System.out.println("Collection: size "+col.size());
            String[] ret = new String[col.size()];
            int x = 0;
            for (Object s : col) {
                ret[x] = (String)s;
                ++x;
            }
            return ret;
        }

        return new String[0];
    }
	
	private ArrayList<Inet4Address> getInet4Addresses() {
		ArrayList<Inet4Address> addresses = new ArrayList<Inet4Address>();
		try {
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			while(interfaces.hasMoreElements()) {
	           NetworkInterface ni = (NetworkInterface) interfaces.nextElement();

	           if (!ni.isLoopback()) {
	        	   Enumeration<InetAddress> e2 = ni.getInetAddresses();
	        	   while (e2.hasMoreElements()){
	        		   InetAddress ip = (InetAddress) e2.nextElement();
	        		   if (ip instanceof Inet4Address) {
	        			   addresses.add((Inet4Address) ip);
	        		   }
	        	   }
	           }
	        }
		} catch (SocketException e1) {
			e1.printStackTrace();
		}

        return addresses;
	}
	
	private Runnable pollSLP() {
		return new Runnable() {
			
			@Override
			public void run() {
                Thread.currentThread().setContextClassLoader(m_classLoader);
                
				UserAgentClient uac = SLP.newUserAgentClient(m_slpUserSettings);
				ServiceType serviceType = new ServiceType("service:osgi.remote:http");
                String language = null;
                Scopes scopes = Scopes.NONE;
                String filter = null;
                try {
                    List<ServiceInfo> services = uac.findServices(serviceType, language, scopes, filter);

                    List<ServiceURL> handled = new ArrayList<ServiceURL>();
                    for (ServiceInfo si : services) {
                    	handled.add(si.getServiceURL());
                        ServiceURL serviceURL = si.getServiceURL();
                        if (!m_importedEndpoints.containsKey(serviceURL)) {
                        	System.out.println("DISCOVERY: Handle new service: " + serviceURL.toString());
                        
                            Hashtable<String, Object> descriptionProps = new Hashtable<String, Object>();
                            Attributes attributes = si.getAttributes();
                            for (String key : attributes) {
                            	Value value = attributes.valueFor(key);

                            	if (RemoteConstants.SERVICE_IMPORTED_CONFIGS.equals(key)) {
                    				Object[] values = value.getValues();
                    				String[] stringArray = Arrays.copyOf(values, values.length, String[].class);
                    				descriptionProps.put(RemoteConstants.SERVICE_IMPORTED_CONFIGS, stringArray);
                    			} else if (RemoteConstants.ENDPOINT_FRAMEWORK_UUID.equals(key)) {
                    				String uuid = (String) value.getValue();
                    				descriptionProps.put(RemoteConstants.ENDPOINT_FRAMEWORK_UUID, uuid);
                    			} else if (RemoteConstants.ENDPOINT_ID.equals(key)) {
                    				// Ignore the ID tag, use the information from the service url itself.
                    				//   This includes the hostname/address, the ID doesn't
                    				// String id = (String) value.getValue();
                    				
                    				String prefix = "service:osgi.remote:";
    	                            String url = serviceURL.getURL().substring(prefix.length());
                    				
                    				descriptionProps.put(RemoteConstants.ENDPOINT_ID, url);
                    			} else if (RemoteConstants.ENDPOINT_SERVICE_ID.equals(key)) {
                    				Long serviceId = new Long((Integer) value.getValue());
                    				if (serviceId != null) {
                    					descriptionProps.put(RemoteConstants.ENDPOINT_SERVICE_ID, serviceId);
                    				}
                    			} else if (Constants.OBJECTCLASS.equals(key)) {
                    				String[] stringArray = Arrays.copyOf(value.getValues(), value.getValues().length, String[].class);
                    				descriptionProps.put(Constants.OBJECTCLASS, stringArray);
                    			} else if (key.equals(Constants.SERVICE_ID)){
                    				Long serviceId = new Long((Integer) value.getValue());
                    				if (serviceId != null) {
                    					descriptionProps.put(Constants.SERVICE_ID, serviceId);
                    				}
                    			} else {
                    				// All other entries can be of any type, so just get the bytes and convert to an Object.
                    				// This includes the import configuration specific settings and the import property itself
                    				//   as well as any other entry added to the service registration.
                    				Object configurationValue = attributes.valueFor(key).getValue();
        							Object object = toObject((byte[]) configurationValue);
        							descriptionProps.put(key, object);
                    			}
                            }

                			EndpointDescription endpointDescription = new EndpointDescription(descriptionProps);

                			informListenersOfAddition(endpointDescription);
                            
                            m_importedEndpoints.put(serviceURL, endpointDescription);
                        }
                    }
                    
                    Iterator<ServiceURL> iter = m_importedEndpoints.keySet().iterator();
                    while (iter.hasNext()) {
                    	ServiceURL serviceURL = iter.next();
						if (!handled.contains(serviceURL)) {
							System.out.println("DISCOVERY: Remove old: " + serviceURL.toString());
							EndpointDescription endpointDescription = m_importedEndpoints.get(serviceURL);
							informListenersOfRemoval(endpointDescription);
							iter.remove();
						}
					}
                    
                }
                catch (Exception e) {
                	e.printStackTrace();
                	
                }
			}
		};
	}

	@Override
	public List<EndpointDescription> getImportedEndpoints() {
		List<EndpointDescription> imports = new ArrayList<EndpointDescription>();
		imports.addAll(m_importedEndpoints.values());
		return imports;
	}

	@Override
	public List<EndpointDescription> getExportedEndpoints() {
		List<EndpointDescription> exports = new ArrayList<EndpointDescription>();
		for (EndpointDescription endpoint : m_exportedEndpoints.keySet()) {
			exports.add(endpoint);
		}
		return exports;
	}

}
