package org.amdatu.remoteservices.demo.chat.impl;

import java.util.Properties;

import org.amdatu.remoteservices.demo.chat.api.MessageReceiver;
import org.amdatu.remoteservices.demo.chat.api.MessageSender;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.CommandProcessor;
import org.osgi.framework.BundleContext;
import org.osgi.service.remoteserviceadmin.RemoteConstants;


public class Activator extends DependencyActivatorBase {
	
	@Override
	public void init(BundleContext context, DependencyManager manager)
			throws Exception {
		
		
		String userName = context.getProperty("chat.name");
		String hostName = java.net.InetAddress.getLocalHost().getHostName();
		
		Properties props = new Properties();
		props.put(RemoteConstants.SERVICE_EXPORTED_INTERFACES, MessageReceiver.class.getName());
		props.put(CommandProcessor.COMMAND_SCOPE, "chat");
        props.put(CommandProcessor.COMMAND_FUNCTION, new String[] { "send" });
		props.put("chat.name", userName + "@" + hostName);

		manager.add(createComponent()
			.setInterface(new String[]{MessageSender.class.getName(), MessageReceiver.class.getName()}, props)
			.setImplementation(ChatClient.class)
			.add(createServiceDependency()
					.setService(MessageReceiver.class)
					.setRequired(false)
					.setCallbacks("addReceiver", "removeReceiver")
					));
	}
	
	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1)
			throws Exception {
	}
}