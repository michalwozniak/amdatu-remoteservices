package org.amdatu.remoteservices.discovery.hazelcast.impl;

import java.io.Serializable;

public class LeaseEvent implements Serializable {

	private static final long serialVersionUID = -6463082608696433348L;
	
	private String m_id;
	private long m_duration;
	
	public LeaseEvent(String id, long duration) {
		m_id = id;
		m_duration = duration;
	}
	
	public String getId() {
		return m_id;
	}
	
	public long getDuration() {
		return m_duration;
	}
}
