package org.amdatu.remoteservices.remotetest.topologymanager.impl;

import org.amdatu.remoteservices.test.topologymanager.Test;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;


public class Activator extends DependencyActivatorBase {
	
	@Override
	public void init(BundleContext context, DependencyManager manager)
			throws Exception {
		manager.add(createComponent()
			.setImplementation(TestConsumer.class)
			.add(createServiceDependency()
					.setService(Test.class)
					.setRequired(false)
					.setCallbacks("add", "remove")
					));
			
	}
	
	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
}