package org.amdatu.remoteservices.discovery.hazelcast.impl;

import java.util.Properties;
import java.util.UUID;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.CommandProcessor;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.log.LogService;
import org.osgi.service.remoteserviceadmin.EndpointListener;
import org.osgi.service.remoteserviceadmin.RemoteConstants;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {
		Properties properties = new Properties();
		properties.put("discovery", true);
		properties.setProperty(EndpointListener.ENDPOINT_LISTENER_SCOPE, 
				"(&(" + Constants.OBJECTCLASS + "=*)(" + RemoteConstants.ENDPOINT_FRAMEWORK_UUID + "=" + getUUID(context) + "))");
		
		properties.put(CommandProcessor.COMMAND_SCOPE, "hcdiscovery");
        properties.put(CommandProcessor.COMMAND_FUNCTION, new String[] { "members" });
		
		manager.add(createComponent()
			.setImplementation(Discovery.class)
			.setInterface(new String[] { EndpointListener.class.getName(), org.amdatu.remoteservices.discovery.commands.Discovery.class.getName() }, properties)
			.add(createServiceDependency()
				.setService(EndpointListener.class)
				.setCallbacks("endpointListenerAdded", "endpointListenerModified", "endpointListenerRemoved"))
			.add(createServiceDependency()
				.setService(LogService.class)
				.setRequired(false))
			);
	}
	
	@Override
	public void destroy(BundleContext context, DependencyManager manager) throws Exception {
		
	}
	
	private String getUUID(BundleContext bctx) {
        synchronized ("org.osgi.framework.uuid") {
            String uuid = bctx.getProperty("org.osgi.framework.uuid");
            if(uuid==null){
                uuid = UUID.randomUUID().toString();
                System.setProperty("org.osgi.framework.uuid", uuid);
            }
            return uuid;
        }
    }

}
