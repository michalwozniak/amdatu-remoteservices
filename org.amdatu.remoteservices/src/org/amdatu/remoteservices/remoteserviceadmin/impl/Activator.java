package org.amdatu.remoteservices.remoteserviceadmin.impl;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdmin;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager)
			throws Exception {
		Component cmp = createComponent()
			.setImplementation(RemoteServiceAdminImpl.class)
			.setInterface(RemoteServiceAdmin.class.getName(), null);
		
		manager.add(cmp);
	}
	
	@Override
	public void destroy(BundleContext context, DependencyManager manager)
			throws Exception {
	}
	
}
