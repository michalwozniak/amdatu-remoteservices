package org.amdatu.remoteservices.discovery.slp.test;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.amdatu.remoteservices.discovery.slp.SlpDiscoveryConstants;
import org.amdatu.remoteservices.test.IntegrationTestBase;
import org.apache.felix.dm.Component;
import org.livetribe.slp.Attributes;
import org.livetribe.slp.SLP;
import org.livetribe.slp.Scopes;
import org.livetribe.slp.ServiceInfo;
import org.livetribe.slp.ServiceType;
import org.livetribe.slp.ServiceURL;
import org.livetribe.slp.sa.ServiceAgent;
import org.livetribe.slp.settings.Keys;
import org.livetribe.slp.settings.MapSettings;
import org.livetribe.slp.ua.UserAgentClient;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.EndpointListener;
import org.osgi.service.remoteserviceadmin.RemoteConstants;

public class DiscoverySlpTest extends IntegrationTestBase implements EndpointListener {
	
	public DiscoverySlpTest() {
		ClassLoader classLoader = this.getClass().getClassLoader();
		Thread.currentThread().setContextClassLoader(classLoader);
	}

	protected Component[] getDependencies() {
        return new Component[] {
                // create Dependency Manager components that should be started before the
                // test starts.
                createComponent()
                    .setImplementation(this)
                    .add(createServiceDependency()
                        .setService(EndpointListener.class)
                        .setRequired(true))
        };
    } 
	
	private EndpointListener m_listener;
	
	private volatile CountDownLatch m_added;
	private volatile CountDownLatch m_removed;
	
	private EndpointDescription m_endpoint;
	
	private ServiceAgent m_serviceAgent;

	private Component m_epl;
	
	@Override
	protected void before() throws Exception {
		super.before();
		// Configure SLP Service
		configure("org.amdatu.remoteservices.discovery.slp", "ars.slp.poll.interval", "1", Keys.PORT_KEY.getKey(), "8091", Keys.UA_UNICAST_PREFER_TCP.getKey(), "true");
		
	}
	
	private void beforeTest() {
		MapSettings m_slpUserSettings = new MapSettings();
		m_slpUserSettings.put(Keys.PORT_KEY, Keys.PORT_KEY.convert(8091));
		m_slpUserSettings.put(Keys.UA_UNICAST_PREFER_TCP, Keys.UA_UNICAST_PREFER_TCP.convert(true));
		
		m_serviceAgent = SLP.newServiceAgent(m_slpUserSettings);
		m_serviceAgent.start();
		
		String scope = "(&(" + Constants.OBJECTCLASS + "=*)(!(" + RemoteConstants.ENDPOINT_FRAMEWORK_UUID + "=" + getUUID(m_bundleContext) + ")))";
		Properties props = new Properties();
    	props.setProperty(ENDPOINT_LISTENER_SCOPE, scope);
    	m_epl = createComponent()
        .setImplementation(this)
        .setInterface(EndpointListener.class.getName(), props);
    	m_dependencyManager.add(m_epl);
    	
	}
	
	private void afterTest() {
		m_dependencyManager.remove(m_epl);
		
		if (m_serviceAgent != null && m_serviceAgent.isRunning()) {
			m_serviceAgent.stop();
		}
		m_serviceAgent = null;
	}
	
	/**
	 * Tests the SLPDiscoveryService with a configuration update. This also implicitly tests the start behaviour.
	 * 
	 * @throws Exception
	 */
	public void testUpdateConfiguration() throws Exception {
		beforeTest();

		// First add an registration
		Hashtable<String, Object> descriptionProps = new Hashtable<String, Object>();
		descriptionProps.put(Constants.OBJECTCLASS, new String[] {"interface2"});
    	String alias = "/service/2/interface";
		descriptionProps.put(RemoteConstants.ENDPOINT_ID, alias);
		descriptionProps.put(RemoteConstants.ENDPOINT_SERVICE_ID, 2l);
		descriptionProps.put(RemoteConstants.ENDPOINT_FRAMEWORK_UUID, "1-2");
		descriptionProps.put(RemoteConstants.SERVICE_IMPORTED_CONFIGS, new String[] { "ARS" });
		EndpointDescription endpoint = new EndpointDescription(descriptionProps);
		ServiceInfo info = createServiceInfo(endpoint, "localhost");
		registerService(info);
		
		// Wait for the added callback
		m_added = new CountDownLatch(1);
		if (!m_added.await(10, TimeUnit.SECONDS)) {
			fail("Endpoint not added");
		}

		// Update the configuration with changed port
		configure("org.amdatu.remoteservices.discovery.slp", "ars.slp.poll.interval", "1", Keys.PORT_KEY.getKey(), "4242", Keys.UA_UNICAST_PREFER_TCP.getKey(), "true");

		// After the update the old service should be removed, since it has been registered on a different port
		m_removed = new CountDownLatch(1);
		if (!m_removed.await(10, TimeUnit.SECONDS)) {
			fail("Endpoint not removed");
		}
		
		// Update the configuration back to the old port
		configure("org.amdatu.remoteservices.discovery.slp", "ars.slp.poll.interval", "1", Keys.PORT_KEY.getKey(), "8091", Keys.UA_UNICAST_PREFER_TCP.getKey(), "true");
		
		// Verify the service is found again
		m_added = new CountDownLatch(1);
		if (!m_added.await(10, TimeUnit.SECONDS)) {
			fail("Endpoint not added");
		}
		
		// Make sure our registered service is removed
		deregisterService(info);
		m_removed = new CountDownLatch(1);
		if (!m_removed.await(10, TimeUnit.SECONDS)) {
			fail("Endpoint not removed");
		}
		
		afterTest();
	}
	
	public void testServiceDiscovery() throws Exception {
		beforeTest();

		// First add an registration
		Hashtable<String, Object> descriptionProps = new Hashtable<String, Object>();
		descriptionProps.put(Constants.OBJECTCLASS, new String[] {"interface2"});
    	String alias = "/service/2/interface";
		descriptionProps.put(RemoteConstants.ENDPOINT_ID, alias);
		descriptionProps.put(RemoteConstants.ENDPOINT_SERVICE_ID, 2l);
		descriptionProps.put(RemoteConstants.ENDPOINT_FRAMEWORK_UUID, "1-2");
		descriptionProps.put(RemoteConstants.SERVICE_IMPORTED_CONFIGS, new String[] { "ARS" });
		EndpointDescription endpoint = new EndpointDescription(descriptionProps);
		ServiceInfo info = createServiceInfo(endpoint, "localhost");
		
		// Wait for the added callback, indicating the service is found
		m_added = new CountDownLatch(1);
		registerService(info);
		if (!m_added.await(10, TimeUnit.SECONDS)) {
			fail("Endpoint not added");
		}
		
        if (m_endpoint != null) {
        	assertTrue(m_endpoint.isSameService(endpoint));
        	assertEquals(m_endpoint.getId(), "http://localhost:8080" + endpoint.getId());
        }
		
		// Remove service
		deregisterService(info);
		
		// Wait for the removed callback, indicating the service is removed
		m_removed = new CountDownLatch(1);
		if (!m_removed.await(10, TimeUnit.SECONDS)) {
			fail("Endpoint not removed");
		}
	}

	public void testEndpointListener() throws Exception {
		beforeTest();
		
		// Create an endpoint description
		Hashtable<String, Object> descriptionProps = new Hashtable<String, Object>();
		descriptionProps.put(Constants.OBJECTCLASS, new String[] {"interface"});
		String alias = "/interface";
		descriptionProps.put(RemoteConstants.ENDPOINT_ID, alias);
		descriptionProps.put(RemoteConstants.ENDPOINT_SERVICE_ID, 42l);
		descriptionProps.put(RemoteConstants.ENDPOINT_FRAMEWORK_UUID, getUUID(m_bundleContext));
		descriptionProps.put(RemoteConstants.SERVICE_IMPORTED_CONFIGS, new String[] { "ARS" });
		EndpointDescription endpointDescription = new EndpointDescription(descriptionProps);
		
		// Invoke the endpointAdded method, triggering the Discovery to publish the service
		m_listener.endpointAdded(endpointDescription, "(" + Constants.OBJECTCLASS + "=interface)");
		
		// Get registered service from SLP and check if the service above has been added
		System.out.println("Check");
		MapSettings m_slpUserSettings = new MapSettings();
		m_slpUserSettings.put(Keys.PORT_KEY, Keys.PORT_KEY.convert(8091));
		m_slpUserSettings.put(Keys.UA_UNICAST_PREFER_TCP, Keys.UA_UNICAST_PREFER_TCP.convert(true));
		UserAgentClient uac = SLP.newUserAgentClient(m_slpUserSettings);
		ServiceType serviceType = new ServiceType("service:osgi.remote:http");
        String language = null;
        Scopes scopes = Scopes.NONE;
        String filter = null;
        List<ServiceInfo> handled = new ArrayList<ServiceInfo>();
        List<ServiceInfo> services = uac.findServices(serviceType, language, scopes, filter);
        ArrayList<Inet4Address> inet4Addresses = getInet4Addresses();
        System.out.println("Check");
        if (services.size() != inet4Addresses.size()) {
        	fail("not the correct number of expected services: expected: " + inet4Addresses.size() + " actual: " + services.size());
        }
        
        for (ServiceInfo serviceInfo : services) {
        	for (Inet4Address inet4Address : inet4Addresses) {
				ServiceInfo createServiceInfo = createServiceInfo(endpointDescription, inet4Address.getHostAddress());
				if (serviceInfo.getServiceURL().equals(createServiceInfo.getServiceURL())) {
					handled.add(serviceInfo);
				}
			}
		}
        services.removeAll(handled);
        if (!services.isEmpty()) {
        	fail("Unexpected services discovered");
        }
		
        // Remove the endpoint
        m_listener.endpointRemoved(endpointDescription, "(" + Constants.OBJECTCLASS + "=interface)");
        
        // Get registered service from SLP and check if the service above has been added
 		services = uac.findServices(serviceType, language, scopes, filter);
 		if (services.size() != 0) {
 			fail("Unexpected services discovered");
 		}
	}
    
    private ArrayList<Inet4Address> getInet4Addresses() {
		ArrayList<Inet4Address> addresses = new ArrayList<Inet4Address>();
		try {
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			while(interfaces.hasMoreElements()) {
	           NetworkInterface ni = (NetworkInterface) interfaces.nextElement();

	           if (!ni.isLoopback()) {
	        	   Enumeration<InetAddress> e2 = ni.getInetAddresses();
	        	   while (e2.hasMoreElements()){
	        		   InetAddress ip = (InetAddress) e2.nextElement();
	        		   if (ip instanceof Inet4Address) {
	        			   addresses.add((Inet4Address) ip);
	        		   }
	        	   }
	           }
	        }
		} catch (SocketException e1) {
			e1.printStackTrace();
		}

        return addresses;
	}
    
    private void registerService(ServiceInfo info) {
    	if (m_serviceAgent.isRunning()) {
    		m_serviceAgent.register(info);
    	}
    }
    
    private void deregisterService(ServiceInfo info) {
    	if (m_serviceAgent.isRunning()) {
    		m_serviceAgent.deregister(info.getServiceURL(), info.getLanguage());
    	}
    }

    private ServiceInfo createServiceInfo(EndpointDescription endpoint, String ipAddress) {
    	
		
		String port = m_bundleContext.getProperty("org.osgi.service.http.port");
		if (port == null) {
			port = "8080";
		}
		String service = endpoint.getId();
		String url = "service:osgi.remote:http://" + ipAddress + ":" + port + service;
		ServiceURL serviceURL = new ServiceURL(url, 10);
        String language = Locale.ENGLISH.getLanguage();
        Scopes scopes = Scopes.DEFAULT;
        
        String scopesS = (String) endpoint.getProperties().get(SlpDiscoveryConstants.SLP_SCOPES);
        if (scopesS != null) {
        	scopes = Scopes.from(scopesS.split(","));
        }
        
        Map<String, String> props = new HashMap<String, String>();
        for (String key : endpoint.getProperties().keySet()) {
			if (RemoteConstants.SERVICE_IMPORTED_CONFIGS.equals(key)) {
				String value = join(endpoint.getConfigurationTypes(), ",");
				props.put(key, value);
			} else if (RemoteConstants.ENDPOINT_FRAMEWORK_UUID.equals(key)) {
				props.put(key, endpoint.getFrameworkUUID());
			} else if (RemoteConstants.ENDPOINT_ID.equals(key)) {
				props.put(key, endpoint.getId());
			} else if (RemoteConstants.ENDPOINT_SERVICE_ID.equals(key)) {
				props.put(key, String.valueOf(endpoint.getServiceId()));
			} else if (Constants.OBJECTCLASS.equals(key)) {
				props.put(key, join(endpoint.getInterfaces(), ","));
			} else if (key.equals(Constants.SERVICE_ID)){
				props.put(key, String.valueOf(endpoint.getServiceId()));
			}
		}
        
        Attributes attributes = Attributes.from(props);
        
        return new ServiceInfo(serviceURL, language, scopes, attributes);
	}
    
    public String join(Iterable<? extends CharSequence> s, String delimiter) {
	    Iterator<? extends CharSequence> iter = s.iterator();
	    if (!iter.hasNext()) return "";
	    StringBuilder buffer = new StringBuilder(iter.next());
	    while (iter.hasNext()) buffer.append(delimiter).append(iter.next());
	    return buffer.toString();
	}
    
	@Override
	public void endpointAdded(EndpointDescription endpoint, String matchedFilter) {
		m_endpoint = endpoint;
		m_added.countDown();
	}

	@Override
	public void endpointRemoved(EndpointDescription endpoint,
			String matchedFilter) {
		m_endpoint = null;
		m_removed.countDown();
	}
	
	private String getUUID(BundleContext bctx) {
        synchronized ("org.osgi.framework.uuid") {
            String uuid = bctx.getProperty("org.osgi.framework.uuid");
            if(uuid==null){
                uuid = UUID.randomUUID().toString();
                System.setProperty("org.osgi.framework.uuid", uuid);
            }
            return uuid;
        }
    }
}
