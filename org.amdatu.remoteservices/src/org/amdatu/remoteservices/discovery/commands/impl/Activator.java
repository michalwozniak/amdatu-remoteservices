package org.amdatu.remoteservices.discovery.commands.impl;

import java.util.Properties;

import org.amdatu.remoteservices.discovery.commands.Discovery;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.CommandProcessor;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {
		Properties properties = new Properties();
		properties.put(CommandProcessor.COMMAND_SCOPE, "discovery");
        properties.put(CommandProcessor.COMMAND_FUNCTION, new String[] { "exported", "imported" });
		
		manager.add(createComponent()
			.setImplementation(DiscoveryCommands.class)
			.setInterface(DiscoveryCommands.class.getName(), properties)
			.add(createServiceDependency()
				.setService(Discovery.class)
				.setCallbacks("discoveryAdded", "discoveryRemoved"))
			);
	}

	@Override
	public void destroy(BundleContext context, DependencyManager manager)
			throws Exception {
	}

}
