package org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl;

import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

public class RestServerEndpoint extends HttpServlet {

	private static final long serialVersionUID = -4909601493830827666L;
	
	private final BundleContext context;
	private final ServiceReference reference;
	private final Class<?>[] proxyInterfaces;

	public RestServerEndpoint(BundleContext context, ServiceReference reference, String[] interfaces) throws ClassNotFoundException {
		this.context = context;
		this.reference = reference;
		this.proxyInterfaces = new Class<?>[interfaces.length];
		for (int i=0;i<interfaces.length;i+=1) {
			this.proxyInterfaces[i] = Class.forName(interfaces[i]);
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Method method = findMethod(req.getPathInfo());
		if (method != null) {
			if (method.getName().startsWith("get") && method.getParameterTypes().length == 0) {		
				processMethod(req, resp, method, null);
			} else {
				resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Requested method does not accessible with GET!");
			}
		} else {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
	}



	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Method method = findMethod(req.getPathInfo());
		if (method != null) {
			Object[] arguments = new Object[method.getParameterTypes().length];
			ObjectMapper mapper = new ObjectMapper();
			JsonNode rootNode = mapper.readValue(req.getInputStream(), JsonNode.class);	
			if (rootNode.size() == arguments.length) {
				int i = 0;
				Iterator<JsonNode> iter = rootNode.getElements();
				while (iter.hasNext()) {
					JsonNode argument = iter.next();
					Class<?> argumentClass = method.getParameterTypes()[i];
					arguments[i] = mapper.readValue(argument, argumentClass);
					i+=1;
				}
				
				processMethod(req, resp, method, arguments);
			} else {
				resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Number of send arguments does not match needed arguments!");
			}
		} else {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
	}
	
	private void processMethod(HttpServletRequest req, HttpServletResponse resp, Method method, Object[] arguments) throws IOException {
		Object service = context.getService(reference);
		if (service != null) {
			try {
				Object result = arguments == null ? method.invoke(service) : method.invoke(service, arguments);
				processResult(req, resp, result);
			} catch (Exception e) {
				resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error processing method: " + e.getMessage());
			} 
		} else {
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Service not available!");
		}
	}
	
	private void processResult(HttpServletRequest req, HttpServletResponse resp, Object result) throws JsonGenerationException, JsonMappingException, IOException {
		if (result != null) {
			ObjectMapper mapper = new ObjectMapper();
			resp.setContentType("application/json");
			mapper.writeValue(resp.getOutputStream(), result);
			resp.getOutputStream().flush();
		}
	}

	private Method findMethod(String requestPath) {
		Method result = null;
		if (requestPath != null && requestPath.length() > 1 && requestPath.charAt(0) == '/') {
			String methodName = requestPath.substring(1);
			for (Class<?> proxyClass : proxyInterfaces) {
				for (Method m: proxyClass.getDeclaredMethods()) {
					if ((m.getModifiers() & Modifier.PUBLIC) > 0 && m.getName().equals(methodName)) {
						result = m;
						break;
					}
				}	
			}
		} 
		
		return result;
	}	
}
