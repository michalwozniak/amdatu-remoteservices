package org.amdatu.remoteservices.discovery.hazelcast.impl;


public class Lease {
	private String m_id;
	private long m_duration;
	private long m_expiration;
	
	public Lease(String id, long duration) {
		m_id = id;
		m_duration = duration;
		renewLease(duration);
	}

	public String getId() {
		return m_id;
	}
	
	public long getDuration() {
		return m_duration;
	}
	
	public synchronized long renewLease(long duration) {
		long time;
		if (duration < 0) {
			time = Long.MAX_VALUE;
		}
		else {
		    time = System.currentTimeMillis() + duration;
		}
		m_expiration = time;
		
		return m_expiration;
	}
	
	public synchronized long getExpiration() {
		return m_expiration;
	}
}
