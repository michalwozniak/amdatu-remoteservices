package org.amdatu.remoteservices.discovery.commands;

import java.util.List;

import org.osgi.service.remoteserviceadmin.EndpointDescription;

public interface Discovery {
	public List<EndpointDescription> getImportedEndpoints();
	public List<EndpointDescription> getExportedEndpoints();
}
