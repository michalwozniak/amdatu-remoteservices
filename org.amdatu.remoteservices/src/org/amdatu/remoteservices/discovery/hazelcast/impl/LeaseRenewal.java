package org.amdatu.remoteservices.discovery.hazelcast.impl;

import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.ITopic;

public class LeaseRenewal {
	
	private HazelcastInstance m_hazelcast;
	
	private ScheduledExecutorService m_executor = Executors.newSingleThreadScheduledExecutor();
	
	private IMap<String, LeaseData> m_leases;
	
	private ITopic<LeaseEvent> m_topic;

	private Map<String, Lease> m_exportLeases;

	public LeaseRenewal(HazelcastInstance hazelcast, Map<String, Lease> exportLeases) {
		m_hazelcast = hazelcast;
		m_exportLeases = exportLeases;

		m_leases = m_hazelcast.getMap(Discovery.LEASES_MAP);
		m_topic = m_hazelcast.getTopic(Discovery.LEASE_TOPIC); 
		
		m_executor.scheduleWithFixedDelay(createRenewalThread(), 0, 5, TimeUnit.SECONDS);
	}
	
	public void stop() {
		m_executor.shutdown();
	}
	
	public Runnable createRenewalThread() {
		return new Runnable() {
			
			@Override
			public void run() {
				ClassLoader ccl = Thread.currentThread().getContextClassLoader();
		        Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
		        
				try {
				for (String key : m_exportLeases.keySet()) {
					Lease lease = m_exportLeases.get(key);
					LeaseData data = m_leases.get(key);
					lease.renewLease(data.getDuration());
					
					m_topic.publish(new LeaseEvent(key, data.getDuration()));
				}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				Thread.currentThread().setContextClassLoader(ccl);
			}
		};
	}
}
