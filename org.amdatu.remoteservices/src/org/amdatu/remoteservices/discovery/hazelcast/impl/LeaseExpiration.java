package org.amdatu.remoteservices.discovery.hazelcast.impl;

import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.ITopic;
import com.hazelcast.core.Message;
import com.hazelcast.core.MessageListener;

public class LeaseExpiration implements MessageListener<LeaseEvent> {
	
	private HazelcastInstance m_hazelcast;
	
	private ScheduledExecutorService m_executor = Executors.newSingleThreadScheduledExecutor();
	
	private ITopic<LeaseEvent> m_topic;
	private Map<String, Lease> m_importLeases;

	private IMap<Object, Object> m_endpointdata;

	public LeaseExpiration(HazelcastInstance hazelcast, Map<String, Lease> importLeases) {
		m_hazelcast = hazelcast;
		m_importLeases = importLeases;

		m_topic = m_hazelcast.getTopic(Discovery.LEASE_TOPIC);
		m_topic.addMessageListener(this);
		
		m_endpointdata = m_hazelcast.getMap(Discovery.ENDPOINTS_MAP);
		
		m_executor.scheduleWithFixedDelay(createExpirationThread(), 0, 1, TimeUnit.SECONDS);
	}
	
	public void stop() {
		m_executor.shutdown();
	}
	
	@Override
	public void onMessage(Message<LeaseEvent> message) {
		ClassLoader ccl = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
        
		Lease lease = m_importLeases.get(message.getMessageObject().getId());
		if (lease != null) {
			synchronized (lease) {
				lease.renewLease(message.getMessageObject().getDuration());
			}
		}
		
		Thread.currentThread().setContextClassLoader(ccl);
		
	}
	
	public Runnable createExpirationThread() {
		return new Runnable() {
			
			@Override
			public void run() {
				ClassLoader ccl = Thread.currentThread().getContextClassLoader();
		        Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
		        
				for (String key : m_importLeases.keySet()) {
					Lease lease = m_importLeases.get(key);
					
					if (lease.getExpiration() < System.currentTimeMillis()) {
						m_endpointdata.remove(key);
					}
				}
				
				Thread.currentThread().setContextClassLoader(ccl);
			}
		};
	}
}
