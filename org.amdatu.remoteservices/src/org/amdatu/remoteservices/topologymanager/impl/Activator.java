package org.amdatu.remoteservices.topologymanager.impl;

import static org.osgi.service.remoteserviceadmin.RemoteConstants.SERVICE_EXPORTED_INTERFACES;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;
import org.osgi.service.remoteserviceadmin.EndpointListener;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdmin;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager)	throws Exception {
		Component newCmp = createComponent()
				.setImplementation(TopologyManagerImpl.class)
				.add(createServiceDependency()
						.setService(LogService.class)
						.setRequired(true))
				.add(createServiceDependency()
						.setService(RemoteServiceAdmin.class)
						.setCallbacks("remoteServiceAdminAdded", "remoteServiceAdminRemoved")
						.setRequired(false))
				.add(createServiceDependency()
						.setService(null, "(" + SERVICE_EXPORTED_INTERFACES + "=*)")
						.setCallbacks("addExportedInterfaces", "removeExportedInterfaces")
						.setRequired(false))
				.add(createServiceDependency()
						.setService(EndpointListener.class)
						.setCallbacks("endpointListenerAdded", "endpointListenerModified", "endpointListenerRemoved")
						.setRequired(false));
		manager.add(newCmp);
	}
	
	@Override
	public void destroy(BundleContext context, DependencyManager manager)
			throws Exception {
	}
	
}
